#!/usr/bin/env python3
"""
..
  Copyright 2020

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import io
import os
import sys
import argparse

import entrezpy.base.result
import entrezpy.base.analyzer
import entrezpy.conduit
import xml.etree.ElementTree as ET

class GBFResult(entrezpy.base.result.EutilsResult):

  def __init__(self, request):
    super().__init__(request.eutil, request.query_id, request.db)
    self.proteins = {}

  def size(self):
    return len(self.proteins)

  def get_link_parameter(self, reqnum=0):
    del reqnum #Unused


  def isEmpty(self):
    if not self.proteins:
      return True
    return False

  def dump(self):
    return {'db':self.db, 'size' : self.size(), 'function' : self.function,
            'proteins': list(self.proteins)}

  def add_protein(self, prot):
    self.proteins.update({prot.acc:prot})


class GBFAnalyzer(entrezpy.base.analyzer.EutilsAnalyzer):

  class Protein:

    def __init__(self):
      self.acc = None
      self.product = None

  def __init__(self):
    super().__init__()

  def init_result(self, response, request:object)->bool:
    if not self.result:
      self.result = GBFResult(request)
      return True
    return False

  def analyze_error(self, response, request:object):
    print (json.dumps({__name__:{'Response-Error':{
                                   'request-dump' : request.dump_internals(),
                                   'error' : response.getvalue()}}}))

  def analyze_result(self, response, request:object):
    self.init_result(response, request)
    prot = self.parsegb(response)
    if prot is not None:
      self.result.add_protein(prot)
      print("{}\t{}".format(prot.acc, prot.product))

  def parsegb(self, response):
    status = 0
    context = iter(ET.iterparse(response, events=("start", "end")))
    event, root = next(context)
    event, elem = next(context)
    p = None
    while True:
      if elem.tag == 'GBSet' and event =='end':
        return None
      if elem.tag == 'GBSeq_locus':
        p = GBFAnalyzer.Protein()
        p.acc = elem.text
      if elem.tag == 'GBSeq' and elem.text =='end':
        status = 0
        p = None
      if elem.tag == 'GBFeature_key' and elem.text =='Protein':
        status = 1
      if status == 1:
        if elem.tag == 'GBQualifier_name' and elem.text =='product':
          event, elem = next(context)
        if elem.tag == 'GBQualifier_value':
          p.product = elem.text
          print("{}\t{}".format(p.acc, p.product))
          status = 0
      event, elem = next(context)

def main():
  ap = argparse.ArgumentParser('poor postdoc efetch')
  ap.add_argument('-e', type=str, help='email address')
  ap.add_argument('-s', type=int, default=5000,
                  help='batch size for one request. Default=5000')
  args = ap.parse_args()

  c = entrezpy.conduit.Conduit(args.e)
  fp = c.new_pipeline()
  fp.add_fetch({'reqsize':args.s, 'db' : 'protein', 'rettype':'gp', 'retmode':'xml', 'id':[x.strip() for x in sys.stdin]},
                analyzer=GBFAnalyzer())
  c.run(fp)
  return 0

if __name__ == '__main__':
  main()
