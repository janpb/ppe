### README

Poor Postdocs Efetch is a poor workaround and a quick and dirty
solution. It can only fetch one thing, the protein product description, from
GenBank XML files.

It prints the accessions and according protein product to STDOUT.

### Note
If it seems the download takes a long time, try to reduce the
batch size, but not too low. Defualt is 5000, try 2000, or 1000.

## Install

Adjust the install directory in the clone step to the direcotry where
you want to run ppe.

```
pip install entrezpy --user
git clone https://github.sydney.edu.au/jpb/ppe install/dir
```

## Usage

```
install/dir/src/ppe.py -h
install/dir/src/ppe.py -e <email> -s <batchsize> < accession.list > output.file
```
